# 
# ball weights here for further reference
ball_w = {
    'red': 0.75,
    'black': 0.25,
    'blue': 0.50
}
# 
# function be like
def minimum_ball_picker(n, m, ball_w=ball_w):
    # 
    # this variable will store number of balls required to meet weight 'm'
    balls = 0
    # 
    # ASSUMPTION: 1 ball of each color exists in the bag of n balls at the very least
    # so
    # if target weight m < (weight of lightest ball), we return -1
    if m < min(ball_w.values()):
        return -1
    # (THIS IS REQUIRED BECAUSE:
    # even though the last return statement handles recursively unachieveable scores, 
    # it doesnt handle impossible scores)
    # 
    # lets convert our bag of balls to an array of weights ;)
    n = [ball_w[x] for x in n]
    n.sort(reverse=True)
    # 
    # while we dont run out of balls in our bag 'n'
    while len(n) != 0:
        # 
        # we're comparing if we still need to go on to get to weight m
        if m - n[0] >= 0:
            # 
            # deduce weight of current max-weight ball in array so far from target weight m
            m -= n[0]
            # 
            # add that we're considering this ball in the solution of 'minimum balls required'
            balls += 1
        # 
        # remove that ball from the sack since we've already considered it
        n.pop(0)
        # 
        # end case
        if m == 0:
            # 
            # when targeted weight m is deduced enough to 0, no need to traverse further
            return balls
    # 
    # if we've run out of balls in bags and score is not attained yet
    if m != 0:
        return -1
# 
# a simple test case - would love to know the result on other test cases that I may have failed to test/cover
print(minimum_ball_picker(['red', 'blue', 'blue', 'blue', 'black', 'black', 'red'], 3.5))
# 
# time complexity here is O(n) (I THINK, its been quite a long time I did DSA haha)
# I couldn't imagine a log-n complexity even if a binary tree was made since traversals could double up based on what we need to get to the score m
# but I could be wrong
# 
# theres other ways this could be done but they are more exaggerated/operation-heavy (for example generating all possible combinations of bag n, 
# putting those combinations as keys in a hashmap, and values is weight and then extract all those that have weight m)
# 
# thats about it :D 