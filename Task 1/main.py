# 
# importies
import random, redis, re, sqlite3
import hashlib as hashfunc
from fastapi import FastAPI
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware
# 
# 
app = FastAPI()
router = InferringRouter()
#
# enabling middleware cause CORS and ReactJS are a bitch pair
origins = [
    "http://localhost:3000"
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
# 
# a class to handle POST-request validation through this model
class UsernameAndPassword(BaseModel):
    Username: str
    Password: str
# 
# a class to handle OTP POST-request validation
class UsernameAndOTP(BaseModel):
    Username: str
    OTP: str
# 
# a class for Mewt too (  HAHA GET IT, ^___^`  )
@cbv(router)
class MewtAuth():
    # 
    # 
    def __init__(self):
        # 
        # connect to DB
        self.con = sqlite3.connect('users.db', check_same_thread=False)
        self.cursor = self.con.cursor()
        # 
        # regex patterns to validate username and password
        self.regex_username = re.compile("^[a-zA-Z0-9_]*$")
        self.regex_password = re.compile("^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$")
        # 
        # so that gordon ramsay doesnt go "ITS BLAAAAAAAAAAND"
        self.salt = str(open('salt.txt', 'r').read().strip())
        # (on a serious note, just to salt the password before hashing)
        # 
        # to create OTPs out of ;)
        self.digits = [str(_) for _ in range(10)]
        self.alphas = list('qwertyuiopasdfghjklzxcvbnm'.upper())
        # 
        # redis instance to store/fetch OTP from 
        self.r = redis.StrictRedis(host="localhost", port=6379, password="", decode_responses=True)
    # 
    # check username validity
    def check_username(self, username):
        if self.regex_username.search(username) == None:
            return False
        return True
    # 
    # check password validity
    def check_password(self, password):
        if len(password) < 8:
            return False
        if self.regex_password.search(password) == None:
            return False
        return True
    # 
    # FUNCTION: log user in
    @router.post("/login_user")
    def login_user(self, data: UsernameAndPassword):
        '''
        Input: {}
        Output: {}

        Process:
        1. decodes post-request
        2. runs password and username validations
        3. checks in DB
        4. generates OTP 
        5. returns success/failure json response 
        '''
        # 
        # decode post body
        data = data.dict()
        username = data["Username"]
        password = data["Password"]
        # 
        # run validations
        try:
            assert self.check_username(username)
        except:
            return {'error': 'Username is invalid.'}
        # 
        # 
        try:
            assert self.check_password(password) 
        except:
            return {'error': 'Password is invalid.'}
        # 
        # check in DB if user exists
        # and if exists compare salted hashed versions of passwords
        salted_password = self.salt + password
        salted_hashed_password = hashfunc.md5(salted_password.encode()).hexdigest()
        self.cursor.execute("SELECT password FROM users WHERE username=?", [username])
        cursor_result = self.cursor.fetchone()
        if cursor_result != None:
            saved_hash = str(cursor_result[0])
        else:
            return {'error': 'User does not exist.'}
        # 
        # check if password hashes match
        try:
            assert salted_hashed_password == saved_hash
        except:
            return {'error': 'Password is wroooooong.'}
        # 
        # since we now know that user exists
        # and passwords match, we generate and store OTP in redis for the 2nd step in auth
        # generate OTP
        OTP = "".join(random.sample(self.alphas + self.digits, 5))
        self.r.set("username:" + username, OTP)
        # 
        # returning the OTP there for convenience
        # NOTE: THIS IS WRONG, we wanna send this in email or by phone
        return {'success': 'OTP is ' + OTP}
    
    @router.post("/check_OTP")
    def check_OTP(self, data: UsernameAndOTP):
        # 
        # decode post body
        data = data.dict()
        username = data["Username"]
        OTP = data["OTP"]
        # 
        # check in redis
        stored_OTP = self.r.get('username:' + username)
        if OTP == stored_OTP:
            self.r.delete('username:' + username)
            return {'success': 'login now!'}
        # 
        # 
        return {'error': 'OTP IS WRONNNNNG!'}

app.include_router(router)