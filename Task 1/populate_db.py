# 
# importies!
import sqlite3, random, hashlib
salt = str(open('salt.txt', 'r').read().strip())
# 
# create db and table 
con = sqlite3.connect('users.db')
cursor = con.cursor()
def create_db():
    cursor.execute(""" CREATE TABLE users (
        username TEXT,
        password TEXT
    ) """)
    con.commit()
# 
# insert users (with fake data)
def create_fake_users():
    users = ['pineapple', 'banana', 'pie', 'cake', 'cookie']
    qwerty = list('qwertyuioasdfghjklzxcvbnm')
    password_special = list('~!@#$%^&*()_+')
    nums = [str(_) for _ in range(10)]
    for _ in range(0, 1000):
        password = "".join(random.sample(qwerty, 6) + random.sample(password_special, 2) + random.sample(nums, 3))
        salted_password = salt + password
        salted_hashed_password = hashlib.md5(salted_password.encode()).hexdigest()
        username = "_".join(random.sample(users, 2) + random.sample(qwerty, 4) + random.sample(nums, 2))
        cursor.execute("INSERT INTO users VALUES (?,?)", (username, salted_hashed_password))
    cursor.execute("INSERT INTO users VALUES (?,?)", ('janedoe', hashlib.md5((salt + 'janedoepassw0rd!').encode()).hexdigest()))
    con.commit()
# 
# sanity check
# create_db()
# create_fake_users()
cursor.execute("SELECT password FROM users WHERE username=?", ['janedoe'])
print(cursor.fetchone())
print(str(cursor.fetchone()[0]))
